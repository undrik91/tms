import pytest
from selenium.webdriver.common.by import By


@pytest.mark.parametrize("product", (i for i in range(1, 5)))
def test_product_info(driver, get_page, auth, product):
    prod_name = driver.find_element(By.CSS_SELECTOR,
                                    f".inventory_item:nth-child({product})"
                                    f" .inventory_item_name")
    prod_price = driver.find_element(By.CSS_SELECTOR,
                                     f".inventory_item:nth-child({product})"
                                     f" .inventory_item_price")
    print(prod_name.text, prod_price.text)


"""
Для всех остальных элементов создать методы,
 которые находят их на странице
и возвращают элемент
"""

# Locators дригих ссылок

BURGER_BUTTON = "#react-burger-menu-btn"
SHOPPING_CART_LINK = ".shopping_cart_link"
PRODUCT_SORT_CONTAINER = ".product_sort_container"


@pytest.mark.parametrize("element", [BURGER_BUTTON, SHOPPING_CART_LINK,
                                     PRODUCT_SORT_CONTAINER])
def test_left_elements(driver, get_page, auth, element):
    driver.find_element(By.CSS_SELECTOR, element)

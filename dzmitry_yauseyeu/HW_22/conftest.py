import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By


URL = "https://www.saucedemo.com"


@pytest.fixture(scope="session")
def driver():
    driver = webdriver.\
        Chrome(executable_path="C:/Dim/chromedriver_win32/chromedriver")
    yield driver
    driver.quit()


@pytest.fixture(scope="session")
def get_page(driver):
    return driver.get(URL)


"""
Нужно найти локаторы для окна Login и залогиниться
"""

# Locators
USERNAME = "user-name"
PASSWORD = "password"
LOGIN_BUTTON = "login-button"


@pytest.fixture(scope="session")
def auth(driver, get_page):
    driver.find_element(By.ID, USERNAME).send_keys("standard_user")
    driver.find_element(By.ID, PASSWORD).send_keys("secret_sauce")
    driver.find_element(By.ID, LOGIN_BUTTON).click()

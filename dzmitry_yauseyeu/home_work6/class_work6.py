from _datetime import datetime


# Functions
"""1) Напишите функцию, которая возвращает строку: “Hello world!”"""


def function_1():
    return "Hello world!"


print(function_1())


"""2) Напишите функцию, которая вычисляет сумму трех чисел и возвращает
результат в основную ветку программы."""


def function_2(a, b, c):
    summ = a + b + c
    return summ


d = function_2(1, 2, 3)
print(d)


"""3) Придумайте программу, в которой из одной функции вызывается вторая.
При этом ни одна из них ничего не возвращает в основную ветку программы,
обе должны выводить результаты своей работы с помощью функции print()."""


def function_3():
    print("This is result of Function 3")


def function_33():
    print("This is result of Function 33")
    function_3()


function_33()


"""4) Напишите функцию, которая не принимает отрицательные числа. и возвращает
число наоборот.
21445 => 54421
123456789 => 987654321"""


def reverse_number(number):
    if number < 0:
        print("Entered value is less than 0")
    else:
        return str(number)[::-1]


number_to_reverse = int(input("Enter number to reverse: "))
print(reverse_number(number_to_reverse))


"""5) Напишите функцию fib(n), которая по данному целому неотрицательному n
возвращает n-e число Фибоначчи."""


def fibonacci(n):
    if n == 1:
        return 0
    elif n in (2, 3):
        return 1
    return fibonacci(n - 1) + fibonacci(n - 2)


place_of_element = int(input("Enter place of the number in Fibonacci list: "))
print(fibonacci(place_of_element))


"""6) Напишите функцию, которая проверяет на то, является ли строка
палиндромом или нет. Палиндром — это слово или фраза, которые одинаково
читаются слева направо и справа налево."""


def palindrome(string_1):
    string_1 = string_1.replace(" ", "")
    string_1 = string_1.lower()
    if string_1 == string_1[::-1]:
        print("This is a palindrome.")
    else:
        print("This is not a palindrome")


string_to_check = "Do geese see God"
palindrome(string_to_check)


"""7) У вас интернет магазин, надо написать функцию которая проверяет что
введен правильный купон и он еще действителен
def check_coupon(entered_code, correct_code, current_date, expiration_date):
    #Code here!
check_сoupon("123", "123", "July 9, 2015", "July 9, 2015")  == True
check_сoupon("123", "123", "July 9, 2015", "July 2, 2015")  == False"""


def check_coupon(entered_code, correct_code, current_date, expiration_date):
    current_date = datetime.strptime(current_date, "%B %d, %Y")
    expiration_date = datetime.strptime(expiration_date, "%B %d, %Y")
    if entered_code == correct_code and expiration_date >= current_date:
        return True
    else:
        return False


result = check_coupon("123", "123", "July 9, 2015", "July 2, 2015")
print(result)

"""8) Фильтр. Функция принимает на вход список, проверяет есть ли эти элементы
в списке exclude, если есть удаляет их и возвращает список с оставшимися
элементами
exclude = ["African", "Roman Tufted", "Toulouse", "Pilgrim", "Steinbacher"]
filter_list(["Mallard", "Hook Bill", "African", "Crested", "Pilgrim",
"Toulouse", "Blue Swedish"]) => ["Mallard", "Hook Bill", "Crested",
"Blue Swedish"]
["Mallard", "Barbary", "Hook Bill", "Blue Swedish", "Crested"] =>
["Mallard", "Barbary", "Hook Bill", "Blue Swedish", "Crested"]
["African", "Roman Tufted", "Toulouse", "Pilgrim", "Steinbacher"] => []"""


def filter_overlap(each_string):
    exclude = ["African", "Roman Tufted", "Toulouse", "Pilgrim",
               "Steinbacher"]
    return each_string not in exclude


filter_list = ["African", "Roman Tufted", "Toulouse", "Pilgrim",
               "Steinbacher"]

print(list(filter(filter_overlap, filter_list)))

from selenium.webdriver.common.by import By
from book_page import BookPage, SelectBookPage
from books_locators import BookPageLocators


def test_open_books_page(driver):
    page = BookPage(driver)
    page.open_page()
    page.open_books()

    assert "/books_2/" in driver.current_url,\
        "Incorrect page has been opened"
    assert "Books" in driver.title


def test_selected_book(driver):
    page = SelectBookPage(driver)
    page.open_page()
    page.open_books()
    page.select_book()
    page.find_book_by_name(BookPageLocators.TITLE)
    assert driver.find_element(By.XPATH,
                               "// h1[\"The shellcoder's handbook\"]")
    assert driver.find_element(By.XPATH,
                               "// img[ @ alt ="
                               " \"The shellcoder's handbook\"]")

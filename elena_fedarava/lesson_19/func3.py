def is_spec_symbol(arg):
    """3яя – проверяет, является ли переданный аргумент спец символом"""
    spec = '[@_!#$%^&*()<>?/|}{~:]'
    if arg in spec:
        return True
    else:
        return False

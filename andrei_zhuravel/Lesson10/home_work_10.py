class Flowers:
    """
    Main parent class
    """
    name = None
    cost = None

    def __init__(self, freshness: int, color: str, length: int):
        """
        :param freshness: freshness
        :param color: color
        :param length: length
        """
        self.freshness = freshness
        self.color = color
        self.length = length

    def __str__(self):
        """
        :return: Flower name and color
        """
        return f'{self.color.title()} {self.name.title()}'


class Roses(Flowers):
    """
    Class for Roses flowers
    Parent class -> Flowers
    """
    name = 'rose'
    cost = 15


class Tulips(Flowers):
    """
    Class for Tulips flowers
    Parent class -> Flowers
    """
    name = 'tulip'
    cost = 12


class Violets(Flowers):
    """
    Class for Violets flowers
    Parent class -> Flowers
    """
    name = 'violet'
    cost = 10


class Chamomiles(Flowers):
    """
    Class for Chamomiles flowers
    Parent class -> Flowers
    """
    name = 'chamomile'
    cost = 7


class Accessories:
    """
    Class for flowers bouquets accessories
    """
    freshness = 0
    color = ''

    def __init__(self, name: str, cost: int):
        """
        :param name: Accessory name
        :param cost: Accessory cost
        """
        self.name = name
        self.cost = cost


class Bouquet:
    """
    Current class objects can collect instances from Flowers and Accessories
    classes
    """

    def __init__(self, *flowers):
        """
        :param flowers: Flowers objects
        """
        self.flowers = self.get_flowers_list(*flowers)
        self.accessories = self.get_accessories_list(*flowers)
        self.index = 0

    @staticmethod
    def get_flowers_list(*args):
        """
        :param args: Flowers objects
        :return: Filtered list only with flowers
        """
        return [i for i in args if isinstance(i, Flowers)]

    @staticmethod
    def get_accessories_list(*args):
        """
        :param args: Flowers objects
        :return: Filtered list only with accessories
        """
        return [i for i in args if not isinstance(i, Flowers)]

    def __str__(self):
        """
        :return: String with flowers name and color
        """
        return ', '.join([f'{f.color} {f.name}' for f in self.flowers])

    def __len__(self):
        """
        :return: number of flower in our bouquet expect accessories
        """
        return len(self.flowers)

    def __contains__(self, flower: object):
        """
        :param flower: Flowers object
        :return: True or False
        """
        return flower in self.flowers

    def __iter__(self):
        """
        :return: Iterated object
        """
        return iter(self.flowers)

    def __next__(self):
        """
        :return: Next collection item or error when collection is completed
        """
        try:
            item = self.flowers[self.index]
        except IndexError:
            raise StopIteration()
        self.index += 1
        return item

    def __getitem__(self, index):
        """
        :param index: Number of index
        :return: Value which corresponds specified index
        """
        return self.flowers[index]

    def __int__(self):
        """
        :return: Total freshness of bouquet
        """
        return sum([i.freshness for i in self.flowers])

    def get_total_cost(self):
        """
        :return: Total cost of Bouquet (flowers + accessories)
        """
        flowers_cost = sum(i.cost for i in self.flowers)
        accessories_cost = sum(i.cost for i in self.accessories)
        return flowers_cost + accessories_cost

    def get_common_freshness(self):
        """
        :return: Total freshness of bouquet
        """
        sum_freshness = sum([i.freshness for i in self.flowers])
        return int(sum_freshness / len(self.flowers))

    def search_by_color(self, value: str):
        """
        :param value: Color by which we searching flowers
        :return: List with search results
        """
        search_result = []
        for i in self.flowers:
            if i.color == value:
                search_result.append(i.name)
            else:
                continue
        return ','.join(search_result) if search_result else 'No found'

    def search_by_freshness(self, value: int):
        """
        :param value: Freshness by which we searching flowers
        :return: List with search results
        """
        search_result = []
        for i in self.flowers:
            if i.freshness == value:
                search_result.append(i.name)
            else:
                continue
        return ','.join(search_result) if search_result else 'No found'

    def search_by_cost(self, value: int):
        """
        :param value: Cost by which we searching flowers
        :return: List with search results
        """
        search_result = []
        for i in self.flowers:
            if i.cost == value:
                search_result.append(i.name)
            else:
                continue
        return ','.join(search_result) if search_result else 'No found'

    def sorting_color(self):
        """
        :return: String of sorted by color objects
        """
        sort_list = sorted(self.flowers, key=lambda f: f.color)
        return ', '.join([f'{f.name.title()}' for f in sort_list])

    def sorting_freshness(self):
        """
        :return: String of sorted by freshness objects
        """
        sort_list = sorted(self.flowers, key=lambda f: f.freshness)
        return ', '.join([f'{f.name.title()}' for f in sort_list])

    def sorting_cost(self):
        """
        :return: String of sorted by cost objects
        """
        sort_list = sorted(self.flowers, key=lambda f: f.cost)
        return ', '.join([f'{f.name.title()}' for f in sort_list])

    def sorting_length(self):
        """
        :return: String of sorted by freshness objects
        """
        sort_list = sorted(self.flowers, key=lambda f: f.length)
        return ', '.join([f'{f.name.title()}' for f in sort_list])


def create_flowers() -> tuple:
    """
    :return: Tuple of flowers
    """
    flower_1 = Roses(5, 'white', 30)
    flower_2 = Tulips(7, 'yellow', 18)
    flower_3 = Violets(6, 'white', 25)
    flower_4 = Chamomiles(11, 'orange', 37)
    return flower_1, flower_2, flower_3, flower_4


def create_accessories() -> tuple:
    """
    :return: Tuple of accessories
    """
    accessory_1 = Accessories('paper', 5)
    accessory_2 = Accessories('tape', 2)
    return accessory_1, accessory_2


if __name__ == '__main__':
    # get flowers
    f1, f2, f3, f4 = create_flowers()
    # get accessories
    ac1, ac2 = create_accessories()
    # get bouquet
    bo = Bouquet(f1, f2, f3, f4, ac1, ac2)
    # flower info
    print(f2)
    print(bo)
    # get bouquet dead
    print(bo.get_common_freshness())
    # search by color
    print(bo.search_by_color('white'))
    # search by cost
    print(bo.search_by_cost(7))
    # search by freshness
    print(bo.search_by_freshness(7))
    # get total cost of bouquet
    print(bo.get_total_cost())
    # get separate flower
    for i in bo:
        print(i, end=',')
    print()
    # check, contain our bouquet particular flower
    print(f4 in bo)
    print(next(bo))
    print(next(bo))
    print(next(bo))
    # get number of flowers in bouquet
    print(len(bo))
    # get flower by index
    print(bo[3])
    # get sorting by color
    print(bo.sorting_color())
    # get sorting by freshness
    print(bo.sorting_freshness())
    # get sorting by length
    print(bo.sorting_length())
    # get sorting by cost
    print(bo.sorting_cost())

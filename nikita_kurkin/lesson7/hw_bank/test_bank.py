import unittest

# как правильно импортировать из другой папки
# при глубокой вложенности - я не нашел
from hw_bank import hw_bank


class TestBank(unittest.TestCase):

    def test_positive_1(self):
        self.assertEqual(hw_bank(100000, 3), 121000)

    def test_negative_calculate(self):
        self.assertNotEqual(hw_bank(1000, 5), 5000)

    @unittest.expectedFailure
    def test_incorrect_data_type(self):
        self.assertEqual(hw_bank("a", 2), 200)

    def test_with_zero(self):
        self.assertEqual(hw_bank(0, 0), 0)

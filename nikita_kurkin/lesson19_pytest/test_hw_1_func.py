import pytest
from func_1 import check_register


class TestCheckWordIsLower:

    @pytest.mark.parametrize("word, expectedResult",
                             [
                                 ('AHALAY', False),
                                 ('mohalay', True),
                                 ('1234', False)
                             ])
    def test_parametrize_check_register(self, word, expectedResult):
        assert check_register(word) == expectedResult

import time


def new_some_decorator(random_string):
    print(random_string)

    def some_decorator(func):
        def wrapper(*args):
            print(func.__name__)
            print(*args)
        return wrapper
    return some_decorator


def simple_decorator(func):
    def wrapper(*args):
        print('Before')
        print(func(*args))
        print('After')
    return wrapper


def increase_sum(func):
    def wrapper(*args):
        result = func(*args)
        result += 1
        print(result)
    return wrapper


def upper_letter(func):
    def wrapper(arg):
        print(func(arg).upper())
    return wrapper


def func_name(func):
    def wrapper(arg):
        print(func.__name__)
    return wrapper


def change(func):
    def wrapper(*args):
        digits = [i for i in reversed(func(*args))]
        print(digits)
    return wrapper


def ttimer(func):
    def wrapper(arg):
        start_time = time.process_time()
        func(arg)
        end_time = time.process_time()
        print(f'Func is executed: {end_time - start_time} sec.')
    return wrapper


def typed(type='str'):
    def replace_type_of_args(func):
        def wrapped(*args):
            new_args = tuple()
            if type == 'int':
                for i in args:
                    if not isinstance(i, int):
                        i = float(i)
                    new_args += (i,)
            elif type == 'str':
                for i in args:
                    if not isinstance(i, str):
                        i = str(i)
                    new_args += (i,)
            print(func(*new_args))
        return wrapped
    return replace_type_of_args

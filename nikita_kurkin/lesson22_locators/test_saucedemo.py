from locators import WelcomePageLocators, CatalogPageLocators


def authorization_action(browser):
    browser.get('https://www.saucedemo.com/')
    browser.find_element(*WelcomePageLocators.LOGIN_INPUT)\
        .send_keys('standard_user')
    browser.find_element(*WelcomePageLocators.PASSWORD_INPUT)\
        .send_keys('secret_sauce')
    browser.find_element(*WelcomePageLocators.LOGIN_BUTTON).click()


class TestSauceDemoSite:

    def test_welcome_page(self, browser):
        authorization_action(browser)
        URL = 'https://www.saucedemo.com/inventory.html'
        assert browser.current_url == URL

    def test_catalog_page(self, browser):
        authorization_action(browser)
        all_items_on_page = browser.\
            find_elements(*CatalogPageLocators.PAGE_ITEMS)

        for item in all_items_on_page:
            item_description = item.\
                find_element(*CatalogPageLocators.ITEM_DESCRIPTION).text
            item_price = item.\
                find_element(*CatalogPageLocators.ITEM_PRICE).text
            item_picture = item.find_element(*CatalogPageLocators.ITEM_PICTURE)
            print(f"\n{item_description} costs {item_price}."
                  f" Picture is allowed: {item_picture.is_enabled()}")

        assert browser.find_element(*CatalogPageLocators.MENU_BUTTON)
        print("Menu button is founded")
        assert browser.find_element(*CatalogPageLocators.FILTER_SELECT)
        print("Filter select is founded")
        assert browser.find_element(*CatalogPageLocators.BIN_BUTTON)
        print("Bin button is founded")
        assert browser.find_element(*CatalogPageLocators.TWITTER_LINK)
        print("Twitter link is founded")
        assert browser.find_element(*CatalogPageLocators.FACEBOOK_LINK)
        print("Facebook link is founded")
        assert browser.find_element(*CatalogPageLocators.LINKEDIN_LINK)
        print("LinkedIn is founded")

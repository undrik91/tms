import unittest

from calculator import find_sum, find_diff, find_multiple, find_div


class TestFirstMethod(unittest.TestCase):

    def test_positive_1(self):
        self.assertEqual(find_sum(2, 2), 4)

    def test_positive_2(self):
        self.assertEqual(find_sum(-1, 4), 3)

    @unittest.expectedFailure
    def test_negative_1(self):
        self.assertEqual(find_sum(1, 1), 3)


class TestSecondMethod(unittest.TestCase):

    def test_positive_1(self):
        self.assertEqual(find_diff(2, 2), 0)

    def test_positive_2(self):
        self.assertEqual(find_diff(-3, -4), 1)

    @unittest.expectedFailure
    def test_negative_1(self):
        self.assertEqual(find_diff(1, 1), 2)


class TestThirdMethod(unittest.TestCase):

    def test_positive_1(self):
        self.assertEqual(find_multiple(2, 0), 0)

    def test_positive_2(self):
        self.assertEqual(find_multiple(3, 4), 12)

    @unittest.expectedFailure
    def test_negative_1(self):
        self.assertEqual(find_multiple("a", 1), 2)


class TestTFourthMethod(unittest.TestCase):

    def test_positive_1(self):
        self.assertEqual(find_div(2, 1), 2)

    def test_positive_2(self):
        self.assertEqual(find_div(4, 4), 1)

    @unittest.expectedFailure
    def test_negative_1(self):
        self.assertEqual(find_div("a", 1), 2)

    # def test_negative_2(self):
    #     with self.assertRaises(ZeroDivisionError):
    #         find_div(2, 0)


if __name__ == "__main__":
    unittest.main()

import string

# IF
# 1. Если значение некой переменной больше 0, выводилось бы специальное
# сообщение. Один раз выполните программу при значении переменной больше 0,
# второй раз — меньше 0.
first_task_int = int(input('Input the number: '))
if first_task_int > 0:
    print('Number > 0')

# 2. Усовершенствуйте предыдущий код с помощью ветки else так, чтобы в
# зависимости от значения переменной, выводилась либо 1, либо -1.

first_task_int = int(input('Input the number: '))
if first_task_int > 0:
    print('Number > 0')
else:
    print('Number < 0')

# 3. Самостоятельно придумайте программу, в которой бы использовалась
# инструкция if (желательно с веткой elif).
# Вложенный код должен содержать не менее трех выражений.

third_task_int = int(input('Input your age: '))
if third_task_int < 18:
    third_task_if_int = 18 - third_task_int
    print(f'Wait for {third_task_if_int} years  to buy some beer')
    print('And go to school without beer!')
elif third_task_int == 18:
    print('Great! Now you are old enought to buy some beer!')
    third_task_int_18 = input('What beeer do you prefer?')
    print('oh!i like it too!')
else:
    print('You are too old to drink beer')
    third_task_int_over_18 = third_task_int - 18
    print(f'{third_task_int_over_18} years ago it could be a great idea')

# ELIF
# 1. Напишите программу по следующему описанию:
# a. двум переменным присваиваются числовые значения;
# b. если значение первой переменной больше второй, то найти разницу
# значений переменных (вычесть из первой вторую), результат связать с
# третьей переменной;
# c. если первая переменная имеет меньшее значение, чем вторая, то третью
# переменную связать с результатом суммы значений двух первых переменных;
# d. во всех остальных случаях, присвоить третьей переменной значение
# первой переменной;
# e. вывести значение третьей переменной на экран.

task_elif_1 = 5
task_elif_2 = 10
if task_elif_1 > task_elif_2:
    task_elif_3 = task_elif_1 - task_elif_2
elif task_elif_2 > task_elif_1:
    task_elif_3 = task_elif_1 + task_elif_2
else:
    task_elif_3 = task_elif_1
print(task_elif_3)

# 2. Придумайте программу, в которой бы использовалась инструкция
# if-elif-else. Количество ветвей должно быть как минимум четыре.

task_elif_4 = int(input('Input your birthyear '))
if task_elif_4 == 1991:
    print('I was born in 1991 too')
elif task_elif_4 == 1968:
    print('You were born in my mothers birthyear')
elif task_elif_4 == 1992:
    print(f'A lot of my friends were born in {task_elif_4}')
else:
    print(f'I dont know anyone else who was born in {task_elif_4}')

# FOR
# 1. Создайте список, состоящий из четырех строк. Затем, с помощью цикла
# for, выведите строки поочередно на экран.

for_task_list = ['hey', 'how', 'hi', 'hip']
for i in for_task_list:
    print(i)

# 2. Измените предыдущую программу так, чтобы в конце каждой буквы строки
# добавлялось тире. (Подсказка: цикл for может быть вложен в другой цикл.)

for_task_list = ['hey', 'how', 'hi', 'hip']
for i in for_task_list:
    for j in i:
        print(j + '-', sep='')

# 3. Создайте список, содержащий элементы целочисленного типа, затем с
# помощью цикла перебора измените тип данных элементов на числа с плавающей
# точкой. (Подсказка: используйте встроенную функцию float().)

for_task_sec_list = [1, 3, 5]
for i in range(len(for_task_sec_list)):
    for_task_sec_list[i] = float(for_task_sec_list[i])
for i in for_task_sec_list:
    print(type(i))

# *4. Есть два списка с одинаковым количеством элементов, в каждом из них
# этих списков есть элемент “hello”. В первом списке индекс этого элемента 1,
# во втором 7.
# Одинаковых элементов в обоих списках может быть несколько. Напишите
# программу которая распечатает индекс слова “hello” в первом списке и
# условный номер списка, индекс слова во втором списки и условный номер
# списка
# со словом Совпадают. Например, программа выведет на экран:
# “Совпадают 1-й элемент из первого списка и 7-й элемент из второго списка”

star_task_1 = [i for i in range(10)]
star_task_1.insert(7, 'hello')
star_task_1 = star_task_1.index('hello')
star_task_2 = [i for i in range(10)]
star_task_2.insert(1, 'hello')
star_task_2 = star_task_2.index('hello')
print(f'Совпадают {star_task_1}-й элемент из первого списка и '
      f'{star_task_2}' + '-й элемент из второго списка')

# WHILE
# 1. Напишите скрипт на языке программирования Python, выводящий ряд чисел
# Фибоначчи (числовая последоватьность в которой числа начинаются с 1 и 1 или
# же и 0 и 1, пример: 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, ..).
# Запустите его на выполнение.# Затем измените код так, чтобы выводился ряд
# # чисел Фибоначчи, начиная с пятого члена ряда и заканчивая двадцатым.

i = 1
j = 1
k = i + j
z_count = 4
while z_count > 0:
    i = j
    j = k
    k = i + j
    z_count = z_count + 1
    if z_count >= 5:
        print(k)
    if z_count == 20:
        break

# 2. Напишите цикл, выводящий ряд четных чисел от 0 до 20. Затем, каждое
# третье число в ряде от -1 до -21.

out_1 = 0
while out_1 != 22:
    print(out_1)
    out_1 += 2
out_1 = -1
while out_1 != -22:
    print(out_1)
    out_1 -= 3

# 3. Самостоятельно придумайте программу на Python, в которой бы
# использовался цикл while. В комментарии опишите что она делает.

out_2 = input('Input the password ')
while out_2 != 'password':
    out_2 = input('Try again!ha-ha-ha ')
print('Great!You are welcome')
# Пока пользователь не угадает "пассворд" система не перестанет спрашивать.

# input()
# Напишите программу, которая запрашивает у пользователя:
# - его имя (например, "What is your name?")
# - возраст ("How old are you?")
# - место жительства ("Where are you live?")
# После этого выводила бы три строки:
# "This is имя"
# "It is возраст"
# "(S)he lives in место_жительства"

user_name = input('What is your name? ')
user_age = input('How old are you? ')
user_city = input('Where are you live? ')
print(f'This is {user_name}')
print(f'User {user_name} is {user_age}')
print(f'{user_name} lives in {user_city}')

# Напишите программу, которая предлагала бы пользователю решить пример 4 *
# 100 - 54. Потом выводила бы на экран правильный ответ и ответ пользователя.
# Подумайте, нужно ли здесь преобразовывать строку в число.

count_diff = input('4*100-54= ')
count_diff_right = 4 * 100 - 54
print(f'Users answer is {count_diff}.Right answer is {count_diff_right}')

# Запросите у пользователя четыре числа. Отдельно сложите первые два и
# отдельно вторые два. Разделите первую сумму на вторую.
# Выведите результат на экран так, чтобы ответ содержал две цифры
# после запятой.

first_int = int(input('Enter first number: '))
sec_int = int(input(('Enter second number: ')))
third_int = int(input(('Enter third number: ')))
fourth_int = int(input(('Enter fourth number: ')))
first_sum = first_int + sec_int
sec_sum = third_int + fourth_int
print(round((first_sum / sec_sum), 2))

# Из заданной строки получить нужную строку:
# "String" => "SSttrriinngg"
# "Hello World" => "HHeelllloo  WWoorrlldd"
# "1234!_ " => "11223344!!__  "

sstring = input('Input a string: ')
for i in sstring:
    print(i * 2, sep='', end='')
print()

# Если х в квадрате больше 1000 - пишем "Hot" иначе - "Nope"
# '50' == "Hot"
# 4 == "Nope"
# "12" == "Nope"
# 60 == "Hot"

while True:
    try:
        x = int(input('Input the number: '))
        break
    except ValueError:
        print('Wrong data input. Try again')
if x * x > 1000:
    print('Hot')
else:
    print('Nope')

# Сложите все числа в списке, они могут быть отрицательными, если список
# пустой вернуть 0
# [] == 0
# [1, 2, 3] == 6
# [1.1, 2.2, 3.3] == 6.6
# [4, 5, 6] == 15
# range(101) == 5050

list_a = range(101)
print(sum(list_a))

# Подсчет букв
# Дано строка и буква => "fizbbbuz","b", нужно подсчитать сколько раз буква b
# встречается
# в заданной строке 3
# "Hello there", "e" == 3
# "Hello there", "t" == 1
# "Hello there", "h" == 2
# "Hello there", "L" == 2
# "Hello there", " " == 1

string_a = input('Input a string: ').lower()
letter_a = input('input the letter you wanna count: ').lower()
print(string_a.count(letter_a))

# Напишите код, который возьмет список строк и пронумерует их.
# Нумерация начинается с 1, имеет : и пробел
# [] => []
# ["a", "b", "c"] => ["1: a", "2: b", "3: c"]

list_b = list(map(str, input('Input elements separated by space: ').split()))
list_c = [str(list_b.index(i) + 1) + ': ' + i for i in list_b]
print(list_c)

# Напишите программу, которая по данному числу n от 1 до n выводит на экран
# n флагов.
# Изображение одного флага имеет размер 4×4 символов. Внутри каждого флага
# должен быть записан
# его номер — число от 1 до n.
# +__
# |x /
# |__\
# |

x = int(input('Input the number: '))
print('+__ ' * x)
for i in range(1, x + 1):
    print(f'|{i} /', end='')
print()
print('|__\\' * x)
print('|   ' * x)

# Напишите программу. Есть 2 переменные salary и bonus. Salary - integer,
# bonus - boolean.
# Если bonus - true, salary должна быть умножена на 10. Если false - нет
# 10000, True == '$100000'
# 25000, True == '$250000'
# 10000, False == '$10000'
# 60000, False == '$60000'
# 2, True == '$20'
# 78, False == '$78'
# 67890, True == '$678900'

salary = int(input('Salary is: '))
bonus_input = (input('Input bonus (True or False): '))
if bonus_input == 'True':
    bonus = True
    print(salary * 10)
elif bonus_input == 'False':
    bonus = False
    print(salary)

# Проверить, все ли элементы одинаковые
# [1, 1, 1] == True
# [1, 2, 1] == False
# ['a', 'a', 'a'] == True
# [] == True

a = list(input('Input elements without separator '))
k = 1
for i in a:
    if i == a[0]:
        k *= 1
    else:
        k *= 0
if k == 1:
    print(True)
else:
    print(False)

# Суммирование. Необходимо подсчитать сумму всех чисел до заданного:
# дано число 2, результат будет -> 3(1+2)
# дано число 8, результат будет -> 36(1 + 2 + 3 + 4 + 5 + 6 + 7 + 8)
# 1 == 1
# 8 == 36
# 22 == 253
# 100 == 5050

input_number = int(input('Input the number: '))
sum_to_input = 0
for i in range(0, input_number + 1):
    sum_to_input += i
print(sum_to_input)

# Проверка строки. В данной подстроке проверить все ли буквы в строчном
# регистре или нет и вернуть список не подходящих.
# dogcat => [True, []]
# doGCat => [False, ['G', 'C']]

input_string = input('Input the string: ')
output_list = [True, []]
for i in input_string:
    if i not in string.ascii_lowercase:
        output_list[0] = False
        output_list[1].append(i)
print(output_list)

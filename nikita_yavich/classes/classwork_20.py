import pytest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys


# driver = webdriver.Chrome(executable_path=
#                           r"C:\\Users\\nikit\\Downloads\\chromedriver_win32"
#                           "\\chromedriver.exe")
# driver.get('https://en.wikipedia.org/wiki/Main_Page')
# search_field = driver.find_element(By.ID, "searchInput")
# search_field.send_keys("Python")
# search_field.send_keys(Keys.ENTER)
# search_header = driver.find_element(By.ID, "firstHeading")
# assert search_header.text == "Search results", "Search page was not opened"
# time.sleep(3)
# driver.quit()


@pytest.fixture()
def test_preparation_open_browser():
    driver = webdriver.Chrome(
        executable_path=r"C:\\Users\\nikit\\Downloads\\chromedriver_win32"
                        "\\chromedriver.exe")
    driver.get('https://en.wikipedia.org/wiki/Main_Page')
    yield driver
    time.sleep(3)
    driver.quit()


def test_search_result(test_preparation_open_browser):
    search_field = test_preparation_open_browser.find_element(By.ID,
                                                              "searchInput")
    search_field.send_keys("Python")
    search_field.send_keys(Keys.ENTER)
    search_header = test_preparation_open_browser.find_element(By.ID,
                                                               "firstHeading")
    assert search_header.text == "Search results", "Search page was not opened"

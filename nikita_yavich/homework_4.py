# 1. Перевести строку в массив
# "Robin Singh" => ["Robin”, “Singh"]
# "I love arrays they are my favorite" => ["I", "love", "arrays", "they",
# "are", "my", "favorite"]

import string

first_string = 'Robin Singh'
second_string = 'I love arrays they are my favorite'
first_list = first_string.split()
second_list = second_string.split()
print(first_list)
print(second_list)

# 2. Дан список: [‘Ivan’, ‘Ivanou’], и 2 строки: Minsk, Belarus
# Напечатайте текст: “Привет, Ivan Ivanou! Добро пожаловать в Minsk Belarus”

sec_ex_list = ['Ivan', 'Ivanou']
sec_ex_first_string = 'Minsk'
sec_ex_sec_string = 'Belarus'
sec_ex_list1, sec_ex_list2 = sec_ex_list
print('Привет,', sec_ex_list1, sec_ex_list2 + '! Добро пожаловать в',
      sec_ex_first_string, sec_ex_sec_string)

# 3. Дан список ["I", "love", "arrays", "they", "are", "my", "favorite"]
# сделайте из него строку => "I love arrays they are my favorite"

third_ex_list = ['I', 'love', 'arrays', 'they', 'are', 'my', 'favorite']
third_ex_string = ' '.join(third_ex_list)
print(third_ex_string)

# 4. Создайте список из 10 элементов, вставьте на 3-ю позицию новое значение,
# удалите элемент из списка под индексом 6

fourth_ex_list = [i for i in range(10)]
fourth_ex_list[2] = 99
del fourth_ex_list[6]
print((fourth_ex_list))

# 5. Есть 2 словаря
# a = { 'a': 1, 'b': 2, 'c': 3}
# b = { 'c': 3, 'd': 4,'e': “”}

a = {'a': 1, 'b': 2, 'c': 3}
b = {'c': 3, 'd': 4, 'e': ''}

# Создайте словарь, который будет содержать в себе все элементы обоих словарей

ab = {**a, **b}
print(ab)

# Обновите словарь “a” элементами из словаря “b”

a.update(b)
print(a)

# Проверить что все значения в словаре “a” не пустые либо не равны нулю

# a_null_check = [i for i in a.values() if not i]
# a_null_check_len = len(a_null_check)
# if a_null_check_len == 0:
#     print('В словаре а нет нулевых и пустых значаений')
# else:
#     print('Количество нулевых и пустых значаений в словаре а: ',
#           a_null_check_len)

# the same in another way:

print('Утверждение, что в словаре а нет пустых либо нулевых значений: ',
      all(a.values()))

# Проверить что есть хотя бы одно пустое значение (результат выполнения
# должен быть True)

# a_null_check = [i for i in a.values() if i == 0 or i == '']
# a_null_check_len = len(a_null_check)
# print(a_null_check_len > 0)

# the same in another way:

print('' in a.values())

# Отсортировать словарь по алфавиту в обратном порядке

for key, value in reversed(a.items()):
    print(key, value)

# Изменить значение под одним из ключей и вывести все значения

a['d'] = 99
print(a)

# 6. Создать список из элементов list_a = [1,2,3,4,4,5,2,1,8]

list_a = [1, 2, 3, 4, 4, 5, 2, 1, 8]

# Вывести только уникальные значения и сохранить их в отдельную переменную

list_b = list_a
list_temp = [list_b.remove(i) for i in list_a if list_a.count(i) > 1]
print(list_b)

# Добавить в полученный объект значение 22

list_b.append(22)
print(list_b)

# Сделать list_a неизменяемым

tuple_a = tuple(i for i in list_a)

# Измерить его длину

print(len(tuple_a))

# Задачи на закрепление форматирования:
# 1 Есть переменные a=10, b=25
# Вывести 'Summ is <сумма этих чисел> and diff = <их разница>.'
# При решении задачи использовать оба способа форматирования

a, b = 10, 25
c = a + b
d = abs(a - b)
print(f'Summ is {c} and diff = {d}')
print('Summ is %s and diff = %d' % (c, d))

# Есть список list_of_children = [“Sasha”, “Vasia”, “Nikalai”]
# Вывести “First child is <первое имя из списка>, second is “<второе>”,
# and last one – “<третье>””

list_of_children = ['Sasha', 'Vasia', 'Nikalai']
print(f'First child is {list_of_children[0]}, second is'
      f' {list_of_children[1]}',
      f', and last one - {list_of_children[2]}')

# *1) Вам передан массив чисел. Известно, что каждое число в этом массиве
# имеет пару, кроме одного: [1, 5, 2, 9, 2, 9, 1] => 5
# Напишите программу, которая будет выводить уникальное число

extra_ex_list = [1, 5, 2, 9, 2, 9, 1]
extra_ex_unical = [i for i in extra_ex_list if extra_ex_list.count(i) == 1]
print(*extra_ex_unical)

# *2) Дан текст, который содержит различные английские буквы и знаки
# препинания. Вам необходимо найти самую частую букву в тексте.
# Результатом должна быть буква в нижнем регистре.
# При поиске самой частой буквы, регистр не имеет значения, так что при
# подсчете считайте, что "A" == "a". Убедитесь, что вы не считайте знаки
# препинания, цифры и пробелы, а только буквы.
# Если в тексте две и больше буквы с одинаковой частотой, тогда результатом
# будет буква, которая идет первой в алфавите. Для примера, "one" содержит
# "o", "n", "e" по одному разу, так что мы выбираем "e".
# "a-z" == "a"
# "Hello World!" == "l"
# "How do you do?" == "o"
# "One" == "e"
# "Oops!" == "o"
# "AAaooo!!!!" == "a"
# "a" * 9000 + "b" * 1000 == "a"

last_ex_text = input().lower()
last_ex_text = tuple(last_ex_text)
letters = tuple(string.ascii_lowercase)
last_ex_dict = {}
last_ex_list = []
for i in letters:
    if last_ex_text.count(i) != 0:
        last_ex_dict[i] = last_ex_text.count(i)
last_ex_dict_max_value = max(last_ex_dict.values())
for i, j in last_ex_dict.items():
    if j == last_ex_dict_max_value:
        last_ex_list.append(i)
last_ex_list = sorted(last_ex_list)
print(last_ex_list[0])

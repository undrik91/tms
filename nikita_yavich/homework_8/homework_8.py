from decorators_8 import dec_abc, second_task_dec, third_dec, fourth_dec, \
    change_dec, timer_dec, func_name_dec, check_n_add


# Напишите декоратор, который печатает перед и после вызова функции слова
# “Before” and “After”


@dec_abc
def abc():
    """
    This function prints 'Hello again!'
    """
    print('Hello again!')


# Напишите функцию декоратор, которая добавляет 1 к заданному числу

@second_task_dec
def second_task(x: int):
    """
    This function takes one argument and returns it.
    """
    return x


# Напишите функцию декоратор, которая переводит полученный текст в верхний
# регистр

@third_dec
def third_task(x: str):
    """
    This function takes one argument and returns it.
    """
    return x


# Напишите декоратор func_name, который выводит на экран имя функции

@fourth_dec('first message inside decorator')
def fourth_task(arg: str):
    """
    This function takes one argument and returns it.
    """
    return arg


# Напишите декоратор change, который делает так, что задекорированная функция
# принимает все свои не именованные аргументы в порядке, обратном тому,
# в котором их передали

@change_dec
def fifth_task(*args):
    """
    This function takes one argument and returns it.
    """
    return args


# Напишите декоратор, который вычисляет время работы декорируемой функции
# (timer)

@timer_dec
def check_task(arg):
    """
    This function takes one argument and returns it. There is a rubbish
    inside function to make it more difficult and check the speed of a
    processor.
    """
    k = 0
    for i in range(10000000):
        k += i
    return arg


# Напишите функцию, которая вычисляет значение числа Фибоначчи для заданного
# количествa элементов последовательности и возвращает его и оберните
# ее декоратором timer и func_name

@timer_dec
@func_name_dec
def fib(arg):
    a = 0
    b = 1
    arg = arg - 2
    for i in range(arg):
        a, b = b, a + b
    return b


# Напишите функцию, которая вычисляет сложное математическое выражение и
# оберните ее декоратором из пункта 1, 2

@dec_abc
@second_task_dec
def difficult(x):
    """
    This function takes an argument and make some mathematic operations with
     it,prints the result and returns it.
    """
    b = x * x + x * x ** x - x ** x
    print(b)
    return b


# Напишите декоратор, который проверял бы тип параметров функции,
# конвертировал их если надо и складывал:
# @typed(type=’str’)
# def add_two_symbols(a, b):
# return a + b

@check_n_add(type='str')
def last_task(*args):
    """
    This function takes arguments and just returns them.
    """
    return args


def menu():
    while True:
        print('Choose the function to run: ',
              '1)Функция с декоратором, который печатает перед и после'
              ' вызова функции слова Before и After',
              '2)Функция с  декоратором, который добавляет 1 к заданному'
              ' числу',
              '3)Функция с  декоратором, который переводит полученный текст'
              ' в верхний регистр',
              '4)Функция с  декоратором, который  выводит на экран имя'
              ' функции',
              '5)Функция с  декоратором, который делает так, '
              'что задекорированная функция принимает все свои не '
              'именованные '
              'аргументы в порядке, обратном тому, в котором их передали',
              '6)Функция с  декоратором, который вычисляет время работы'
              ' декорируемой функции',
              '7)Функция, которая вычисляет значение числа Фибоначчи для '
              'заданного количествa элементов последовательности и '
              'возвращает его, с декораторами timer и func_name',
              '8)Функция, которая вычисляет сложное математическое выражение'
              ' с декораторами из пункта 1, 2',
              '9)Функция с  декоратором, который проверял бы тип параметров '
              'функции,конвертировал их, если надо, и складывал', sep='\n'
              )
        menu_choice = input()
        if menu_choice not in ['1', '2', '3', '4', '5', '6', '7', '8', '9']:
            print('Try again')
            continue
        else:
            break

    if menu_choice == '1':
        abc()
    elif menu_choice == '2':
        print(second_task(10))
    elif menu_choice == '3':
        print(third_task('hello again'))
    elif menu_choice == '4':
        print(fourth_task(10, 11, 12))
    elif menu_choice == '5':
        print(fifth_task('a', 'b', 'c'))
    elif menu_choice == '6':
        print(check_task('a'))
    elif menu_choice == '7':
        print(fib(70000))
    elif menu_choice == '8':
        print(difficult(33))
    elif menu_choice == '9':
        print(last_task(10, '20'))


menu()

import json

"""
Работа с json файлом
Разработайте поиск учащихся в одном классе, посещающих одну секцию.
Фильтрацию учащихся по их полу. Поиск ученика по имени(часть имени)
"""


def search_by_section_and_class(class_name, section):
    with open('students.json') as json_data:
        data = json.load(json_data)
    names_list = []
    for row in data:
        if section in row['Club'] and class_name in row['Class']:
            names_list.append(row['Name'])
    return names_list


def filter_by_gender(gender):
    with open('students.json') as json_data:
        data = json.load(json_data)
    names_list = []
    for row in data:
        if gender in row['Gender']:
            names_list.append(row['Name'])
    return names_list


def search_by_name(name):
    with open('students.json') as json_data:
        data = json.load(json_data)
    names_list = []
    for row in data:
        if name in row['Name']:
            names_list.append(row['Name'])
    return names_list


if __name__ == "__main__":
    print(search_by_section_and_class("5a", "Football"))

from collections import Counter
import string


"""
Перевести строку в массив
"Robin Singh" => ["Robin”, “Singh"]
"I love arrays they are my favorite" =>
["I", "love", "arrays", "they", "are", "my", "favorite"]
"""
str1 = "Robin Singh"
str2 = "I love arrays they are my favorite"
print(str1.split(' '))
print(str2.split(' '))


"""
Дан список: [‘Ivan’, ‘Ivanou’], и 2 строки: Minsk, Belarus
Напечатайте текст: “Привет, Ivan Ivanou! Добро пожаловать в Minsk Belarus”
"""
arr1 = ["Ivan", "Ivanou"]
str3 = "Minsk"
str4 = "Belarus"
print(f"Привет, {' '.join(arr1)}! Добро пожаловать в {str3} {str4}")


"""
Дан список ["I", "love", "arrays", "they", "are", "my", "favorite"]
сделайте из него строку => "I love arrays they are my favorite"
"""
rea = ["I", "love", "arrays", "they", "are", "my", "favorite"]
str5 = ' '.join(rea)
print(str5)


"""
Создайте список из 10 элементов, вставьте на 3-ю позицию новое значение,
удалите элемент из списка под индексом 6
"""
arr2 = list(range(10))
arr2.insert(2, "A")
del arr2[6]
print(arr2)


"""
Есть 2 словаря
                 a = { 'a': 1, 'b': 2, 'c': 3}
                 b = { 'c': 3, 'd': 4,'e': “”}
1. Создайте словарь, который будет содержать в себе все элементы обоих словарей
2. Обновите словарь “a” элементами из словаря “b”
3. Проверить что все значения в словаре “a” не пустые либо не равны нулю
4. Проверить что есть хотя бы одно пустое значение (результат выполнения должен
быть True)
5. Отсортировать словарь по алфавиту в обратном порядке
6. Изменить значение под одним из ключей и вывести все значения
"""
a = {'a': 1, 'b': 2, 'c': 3}
b = {'c': 3, 'd': 4, 'e': ""}
merge = {**a, **b}
print(merge)
a.update(b)
print(a)
print(() in a.values())
print(0 in a.values())
print("" in a.values())
print(sorted(a, reverse=True))
a['b'] = 20
print(a)


"""
Создать список из элементов list_a = [1,2,3,4,4,5,2,1,8]
Вывести только уникальные значения и сохранить их в отдельную переменную
Добавить в полученный объект значение 22
Сделать list_a неизменяемым
Измерить его длинну
"""
list_a = [1, 2, 3, 4, 4, 5, 2, 1, 8]
list_b = list(set(list_a))
print(list_b)
list_b.append(22)
print(list_b)
list_a = tuple(list_a)
print(len(list_a))


# Задачи на закрепление форматирования:
"""
Есть переменные a=10, b=25
Вывести 'Summ is <сумма этих чисел> and diff = <их разница>.'
При решении задачи использовать оба способа форматирования
"""
a = 10
b = 25
print("Summ is {} and diff = {}.".format(a + b, a - b))
print(f"Summ is {a + b} and diff = {a - b}.")


"""
Есть список list_of_children = [“Sasha”, “Vasia”, “Nikalai”]
Вывести “First child is <первое имя из списка>, second is “<второе>”,
and last one – “<третье>””
"""
list_of_children = ["Sasha", "Vasia", "Nikalai"]
list1 = ''.join(list_of_children[0])
list2 = ''.join(list_of_children[1])
list3 = ''.join(list_of_children[2])
print(f"First child is {list1}, second is {list2}, and last one – {list3}")


"""
*1) Вам передан массив чисел. Известно, что каждое число в этом массиве имеет
пару, кроме одного: [1, 5, 2, 9, 2, 9, 1] => 5
Напишите программу, которая будет выводить уникальное число
"""
a = [1, 5, 2, 9, 2, 9, 1]
z = Counter(a).most_common()[-1][0]
print(z)


"""
*2) Дан текст, который содержит различные английские буквы и знаки препинания.
Вам необходимо найти самую частую букву в тексте. Результатом должна быть
буква в нижнем регистре. При поиске самой частой буквы, регистр не имеет
значения, так что при подсчете считайте, что "A" == "a". Убедитесь,
что вы не считайте знаки препинания, цифры и пробелы, а только буквы.
Если в тексте две и больше буквы с одинаковой частотой, тогда результатом будет
буква, которая идет первой в алфавите. Для примера, "one" содержит
"o", "n", "e" по одному разу, так что мы выбираем "e".
"a-z" == "a"
"Hello World!" == "l"
"How do you do?" == "o"
"One" == "e"
"Oops!" == "o"
"AAaooo!!!!" == "a"
"a" * 9000 + "b" * 1000 == "a"
"""

# my_program
text = input()
text = text.lower()
for c in string.punctuation:
    text = text.replace(c, "")
    text = text.replace(" ", "")
    text = ''.join([x for x in text if not x.isdigit()])
    text = ''.join(sorted(text))
print(Counter(text).most_common()[0][0])

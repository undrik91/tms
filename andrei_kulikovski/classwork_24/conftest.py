import pytest
from selenium import webdriver


@pytest.fixture(scope="session")
def driver():
    driver = webdriver.Chrome(executable_path="D:/QA python/Python/"
                                              "chromedriver")
    yield driver
    driver.quit()

from pages.base_page import BasePage
from pages.login_or_register_page_locators import LoginOrRegisterPageLocators


class LoginOrRegisterPage(BasePage):
    URL = "https://selenium1py.pythonanywhere.com/en-gb/accounts/login/"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def login(self, username, password):
        print("Successfully login")
        pass

    def register(self, email, password):
        pass

    def get_title(self):
        return self.driver.title

    def should_be_login_title(self):
        return "Login or register" in self.driver.title

    def should_be_login_form(self):
        return self.driver.find_element(
            *LoginOrRegisterPageLocators.LOGIN_FORM)

    def should_be_register_form(self):
        return self.driver.find_element(
            *LoginOrRegisterPageLocators.REGISTER_FORM)

    def should_be_login_page(self):
        if self.should_be_login_title() and\
                self.should_be_register_form() and\
                self.should_be_login_form():
            return True
        return False

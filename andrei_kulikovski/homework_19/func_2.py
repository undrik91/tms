class InputTypeError(Exception):
    pass


def float_num(arg):
    if arg == int(arg):
        return float(arg)
    raise InputTypeError


# print(float_num(145))
# print(float_num("145"))

import unittest
from bank import bank


class TestBank(unittest.TestCase):
    def test_bank_positive(self):
        answer = bank(100, 3)
        self.assertEqual(answer, 133.1)

    def test_bank_negative(self):
        answer = bank(-100, 3)
        self.assertNotEqual(answer, 133.1)

    @unittest.expectedFailure
    def test_bank_str_fail(self):
        answer = bank(100, "r")
        self.assertEqual(answer, 133.1)

    @unittest.expectedFailure
    def test_bank_fail(self):
        answer = bank(100, 3)
        self.assertNotEqual(answer, 133.1)

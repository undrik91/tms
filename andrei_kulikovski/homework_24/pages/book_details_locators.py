from selenium.webdriver.common.by import By


class BookDetailLocators:
    BOOK_IMG = (By.XPATH, '//img')
    BOOK_NAME = (By.XPATH, '//*[@class="col-sm-6 product_main"]/h1')
    BOOK_PRICE = (By.XPATH, '//*[@class="price_color"]')

from calculator_func import find_sum, find_diff, find_multiple, find_div
import unittest


class TestSumFunc(unittest.TestCase):

    def test_sum_of_two_positive_numbers(self):
        self.assertEqual(find_sum(10, 5), 15)

    def test_sum_of_two_negative_numbers(self):
        self.assertEqual(find_sum(-10, -5), -15)

    @unittest.expectedFailure
    def test_sum_of_positive_and_negative_number(self):
        self.assertNotEqual(find_sum(10, -5), 5)


class TestDifferenceFunc(unittest.TestCase):

    def test_diff_of_two_positive_numbers(self):
        self.assertEqual(find_diff(10, 5), 5)

    def test_diff_of_two_negative_numbers(self):
        self.assertEqual(find_diff(-10, -5), -5)

    @unittest.expectedFailure
    def test_diff_of_positive_and_negative_number(self):
        self.assertNotEqual(find_diff(10, -5), 15)


class TestDivisionFunc(unittest.TestCase):

    def test_division_of_two_positive_numbers(self):
        self.assertEqual(find_div(10, 5), 2)

    def test_division_of_two_negative_numbers(self):
        self.assertEqual(find_div(-10, -5), 2)

    @unittest.expectedFailure
    def test_division_of_positive_and_negative_number(self):
        self.assertNotEqual(find_div(10, -5), -2)


class TestMultiplicationFunc(unittest.TestCase):

    def test_multiple_of_two_positive_numbers(self):
        self.assertEqual(find_multiple(10, 5), 50)

    def test_multiple_of_two_negative_numbers(self):
        self.assertEqual(find_multiple(-10, -5), 50)

    @unittest.expectedFailure
    def test_multiple_of_positive_and_negative_number(self):
        self.assertNotEqual(find_multiple(10, -5), -50)


if __name__ == "__main__":
    unittest.main()

from xml.etree import ElementTree

"""
Разработайте поиск книги в библиотеке по ее автору(часть имени)/цене/
заголовку/описанию.
"""

library_file = "library.xml"


def find_book_by_search_parameter():
    search_param = str(input("Please, enter the author name/price/title/"
                             "description of the book you're looking for: "))
    root = ElementTree.parse(library_file).getroot()
    for child in root:
        for i in child:
            if (i.tag == "author" and search_param in i.text) or\
                    (i.tag in ("price", "title", "description") and
                     search_param == i.text):
                print(f"Id of the book you are looking for is "
                      f"{child.attrib['id']}")


find_book_by_search_parameter()

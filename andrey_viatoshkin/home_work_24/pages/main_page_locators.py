from selenium.webdriver.common.by import By


class MainPageLocators:
    BOOKS_LINK_SELECTOR = (By.XPATH, '//a[text()="Books"]')
    SEARCH_INPUT_SELECTOR = (By.XPATH, '//*[@type="search"]')
    SEARCH_BUTTON_SELECTOR = (By.XPATH, '//*[@value="Search"]')
    VIEW_BASKET_SELECTOR = (By.XPATH, '//a[@class="btn btn-default"]')

from andrey_viatoshkin.home_work_24.pages.base_page import BasePage
from andrey_viatoshkin.home_work_24.pages.book_detail_page_locators import\
    BookDetailPageLocators


class BookDetailPage(BasePage):

    def __init__(self, driver, url):
        super().__init__(driver, url)

    def get_book_image(self):
        return self.driver.find_element(
            *BookDetailPageLocators.BOOK_IMAGE_LOCATOR)

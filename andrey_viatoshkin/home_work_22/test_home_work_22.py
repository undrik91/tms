import pytest
import logging
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
logging.basicConfig(filename='hw_22_logfile.log',
                    level=logging.INFO, force=True, filemode='w')
ROBOT_HEADER_LOGO_CSS_SELECTOR = '.bot_column'
TAB_NAME_CSS_SELECTOR = 'title'
ERROR_MESSAGE_CSS_SELECTOR = '[data-test="error"]'
CROSS_ON_ERROR_TOAST_CSS_SELECTOR = '.error-button'
PRODUCTS_HEADER_CSS_SELECTOR = '.title'
BURGER_BUTTON_CSS_SELECTOR = '#react-burger-menu-btn'
ALL_ITEMS_OPTION_CSS_SELECTOR = '#inventory_sidebar_link'
ABOUT_OPTION_CSS_SELECTOR = '#about_sidebar_link'
LOGOUT_OPTION_CSS_SELECTOR = '#logout_sidebar_link'
RESET_APP_STATE_OPTION_CSS_SELECTOR = '#reset_sidebar_link'
CART_LINK_CSS_SELECTOR = '.shopping_cart_link'
COUNT_OF_GOODS_IN_CART_CSS_SELECTOR = '.shopping_cart_badge'
ROBOT_HEAD_LOGO_HEADER_CSS_SELECTOR = '.peek'
SORTING_DROPDOWN_CSS_SELECTOR = '.active_option'
SORT_BY_NAME_ASC_OPTION_CSS_SELECTOR = '[value="az"]'
SORT_BY_NAME_DESC_OPTION_CSS_SELECTOR = '[value="za"]'
SORT_BY_PRICE_ASC_OPTION_CSS_SELECTOR = '[value="lohi"]'
SORT_BY_PRICE_DESC_OPTION_CSS_SELECTOR = '[value="hilo"]'
BACKPACK_LOGO_CSS_SELECTOR = '#item_4_img_link'
BACKPACK_DESCRIPTION_XPATH = "//*[@id='item_4_img_link']//ancestor::" \
                             "*[@class='inventory_item']" \
                             "//*[@class='inventory_item_desc']"
BACKPACK_ADD_TO_CART_CSS_SELECTOR = '#add-to-cart-sauce-labs-backpack'
BACKPACK_REMOVE__FROM_CART_CSS_SELECTOR = '#remove-sauce-labs-backpack'
BIKE_LIGHT_LOGO_CSS_SELECTOR = '#item_0_img_link'
BIKE_LIGHT_DESCRIPTION_XPATH = "//*[@id='item_0_img_link']" \
                               "//ancestor::*[@class='inventory_item']" \
                               "//*[@class='inventory_item_desc']"
BIKE_LIGHT_ADD_TO_CART_CSS_SELECTOR = '#add-to-cart-sauce-labs-bike-light'
BIKE_LIGHT_REMOVE_FROM_CART_CSS_SELECTOR = '#remove-sauce-labs-bike-light'
T_SHORT_BLACK_LOGO_CSS_SELECTOR = '#item_1_img_link'
T_SHORT_BLACK_DESCRIPTION_XPATH = "//*[@id='item_1_img_link']" \
                                  "//ancestor::*[@class='inventory_item']" \
                                  "//*[@class='inventory_item_desc']"
T_SHORT_BLACK_ADD_TO_CART_CSS_SELECTOR = '#add-to-cart-sauce-labs-bolt-t-shirt'
T_SHORT_BLACK_REMOVE_FROM_CART_CSS_SELECTOR = '#remove-sauce-labs-bolt-t-shirt'
JACKET_LOGO_CSS_SELECTOR = '#item_5_img_link'
JACKET_DESCRIPTION_XPATH = "//*[@id='item_5_img_link']" \
                           "//ancestor::*[@class='inventory_item']" \
                           "//*[@class='inventory_item_desc']"
JACKET_ADD_TO_CART_CSS_SELECTOR = '#add-to-cart-sauce-labs-fleece-jacket'
JACKET_REMOVE_FROM_CART_CSS_SELECTOR = '#remove-sauce-labs-fleece-jacket'
ONESIE_LOGO_CSS_SELECTOR = '#item_2_img_link'
ONESIE_DESCRIPTION_XPATH = "//*[@id='item_2_img_link']" \
                           "//ancestor::*[@class='inventory_item']" \
                           "//*[@class='inventory_item_desc']"
ONESIE_ADD_TO_CART_CSS_SELECTOR = '#add-to-cart-sauce-labs-onesie'
ONESIE_REMOVE_FROM_CART_CSS_SELECTOR = '#remove-sauce-labs-onesie'
T_SHORT_RED_LOGO_CSS_SELECTOR = '#item_3_img_link'
T_SHORT_RED_DESCRIPTION_XPATH = "//*[@id='item_3_img_link']//ancestor::" \
                                "*[@class='inventory_item']" \
                                "//*[@class='inventory_item_desc']"
T_SHORT_RED_ADD_TO_CART_CSS_SELECTOR = '#add-to-cart-test.allthethings()' \
                                       '-t-shirt-(red)'
T_SHORT_RED_REMOVE_FROM_CART_CSS_SELECTOR = '#remove-test.allthethings()' \
                                            '-t-shirt-(red)'
TWITER_LINK_CSS_SELECTOR = '.social_twitter [href]'
FACEBOOK_LINK_CSS_SELECTOR = '.social_facebook [href]'
LINKEDIN_LINK_CSS_SELECTOR = '.social_linkedin [href]'
ROBOT_FOOTER_LOGO_CSS_SELECTOR = '.footer_robot'
FOOTER_INFO_CSS_SELECTOR = '.footer_copy'


class TestSuite:

    def test_display_name_and_price(self, driver):
        for count in range(0, 6):
            title = driver.find_element(By.CSS_SELECTOR,
                                        f"#item_{count}_title_link"
                                        f" .inventory_item_name").text
            price = driver.find_element(By.XPATH,
                                        f"//*[@id='item_{count}_img_link']//"
                                        f"ancestor::*[@class='inventory_item']"
                                        "//*[@class='inventory_item_price']")\
                .text
            logging.info(f'{title}, {price}')

    @pytest.mark.parametrize('element_path', [
        PRODUCTS_HEADER_CSS_SELECTOR,
        BURGER_BUTTON_CSS_SELECTOR,
        ALL_ITEMS_OPTION_CSS_SELECTOR,
        ABOUT_OPTION_CSS_SELECTOR,
        LOGOUT_OPTION_CSS_SELECTOR,
        RESET_APP_STATE_OPTION_CSS_SELECTOR,
        CART_LINK_CSS_SELECTOR,
        COUNT_OF_GOODS_IN_CART_CSS_SELECTOR,
        ROBOT_HEAD_LOGO_HEADER_CSS_SELECTOR,
        SORTING_DROPDOWN_CSS_SELECTOR,
        SORT_BY_NAME_ASC_OPTION_CSS_SELECTOR,
        SORT_BY_NAME_DESC_OPTION_CSS_SELECTOR,
        SORT_BY_PRICE_ASC_OPTION_CSS_SELECTOR,
        SORT_BY_PRICE_DESC_OPTION_CSS_SELECTOR,
        TWITER_LINK_CSS_SELECTOR,
        FACEBOOK_LINK_CSS_SELECTOR,
        LINKEDIN_LINK_CSS_SELECTOR,
        ROBOT_FOOTER_LOGO_CSS_SELECTOR,
        FOOTER_INFO_CSS_SELECTOR,
    ])
    def test_find_and_display_element(self, driver, element_path):
        if element_path == COUNT_OF_GOODS_IN_CART_CSS_SELECTOR:
            try:
                driver.find_element(By.CSS_SELECTOR, element_path)\
                    .is_displayed()
            except NoSuchElementException:
                driver.find_element(By.CSS_SELECTOR,
                                    BACKPACK_ADD_TO_CART_CSS_SELECTOR).click()
                element = driver.find_element(By.CSS_SELECTOR, element_path)\
                    .text
                logging.info(f'{element}')
        else:
            element = driver.find_element(By.CSS_SELECTOR, element_path).text
            logging.info(f'{element}')

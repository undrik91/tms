from statistics import mean


# Определить иерархию и создать несколько цветов (Rose, Tulips, violet,
# chamomile). У каждого класса цветка следующие атрибуты:
# Стоимость – атрибут класса, который определяется заранее
# Свежесть (в днях), цвет, длинна стебля – атрибуты экземпляра
# При попытке вывести информацию о цветке, должен отображаться цвет и тип
# Rose = RoseClass(7, Red, 15)

class Rose:
    price = 10

    def __init__(self, name, freshness, color, stalk_length):
        self.freshness = freshness
        self.color = color
        self.stalk_length = stalk_length
        self.name = name

    def __str__(self):
        return f'{self.color}, {Rose.__name__}'


class Violet:
    price = 11

    def __init__(self, name, freshness, color, stalk_length):
        self.freshness = freshness
        self.color = color
        self.stalk_length = stalk_length
        self.name = name

    def __str__(self):
        return f'{self.color}, {Violet.__name__}'


class Chamomile:
    price = 12

    def __init__(self, name, freshness, color, stalk_length):
        self.freshness = freshness
        self.color = color
        self.stalk_length = stalk_length
        self.name = name

    def __str__(self):
        return f'{self.color}, {Chamomile.__name__}'


class Tulips:
    price = 20

    def __init__(self, name, freshness, color, stalk_length):
        self.freshness = freshness
        self.color = color
        self.stalk_length = stalk_length
        self.name = name

    def __str__(self):
        return f'{self.color}, {Tulips.__name__}'


rose = Rose('rose', 100, 'red', 100)
rose_1 = Rose('rose', 100, 'red', 100)
print(rose.__str__())
tulips = Tulips('tulips', 22, 'blue', 30)
tulips_1 = Tulips('atulips_1', 21, 'black', 30)


# Собрать букет (можно использовать аксессуары – отдельные классы со своей
# стоимостью: например, упаковочная бумага) с определением ее стоимости.
# Узнать, есть ли цветок в букете. (__contains__)
# Добавить возможность получения цветка по индексу (__getitem__) либо
# возможность пройтись по букету и получить каждый цветок
# по-отдельности (__iter__)
# У букета должна быть возможность определить время его увядания по
# среднему времени жизни всех цветов в букете
class Bouqet:
    def __init__(self, *flowers):
        self.flowers = flowers

    def __getitem__(self, index):
        return self.flowers[index]

    def __contains__(self, obj):
        return obj in self.flowers

    def __iter__(self):
        return iter(self.flowers)

    @staticmethod
    def bouquet_price(*args):
        bouquet_price_sum = sum([arg.price for arg in args])
        return bouquet_price_sum

    def bouquet_term(*args):
        bouquet_term = mean([arg.freshness for arg in args])
        return bouquet_term

# Позволить сортировку цветов в букете на основе различных параметров
# (свежесть/цвет/длина стебля/стоимость...)
    def bouqet_sort(*args, choice=None):
        bouquet_list = [*args]
        if choice == '1':
            new_list = sorted(bouquet_list, key=lambda x: x.freshness,
                              reverse=True)
            for i in new_list:
                print(i.name)
        elif choice == '2':
            new_list = sorted(bouquet_list, key=lambda x: x.color,
                              reverse=True)
            for i in new_list:
                print(i.name)
        elif choice == '3':
            new_list = sorted(bouquet_list, key=lambda x: x.stalk_length,
                              reverse=True)
            for i in new_list:
                print(i.name)
        elif choice == '4':
            new_list = sorted(bouquet_list, key=lambda x: x.price,
                              reverse=True)
            for i in new_list:
                print(i.name)
        else:
            print('Choice must be 1 or 2 or 3 or 4')

# Реализовать поиск цветов в букете по определенным параметрам.
    def search_flower(*args, choice=None):
        if choice == '1':
            search_criteria = int(input('type freshness '))
            for el in args:
                if search_criteria == el.freshness:
                    return el.name
            else:
                return 'no such flower'
        elif choice == '2':
            search_criteria = input('color ')
            for el in args:
                if search_criteria == el.color:
                    return el.name
            else:
                return 'no such flower'
        elif choice == '3':
            search_criteria = int(input('stalk length '))
            for el in args:
                if search_criteria == el.stalk_length:
                    return el.name
            else:
                return 'no such flower'
        elif choice == '4':
            search_criteria = int(input('flower price '))
            for el in args:
                if search_criteria == el.price:
                    return el.name
            else:
                return 'no such flower'


bouquet = Bouqet
print(bouquet.bouquet_price(rose, tulips, tulips))
print(bouquet.bouquet_term(rose, tulips, tulips, tulips))
bouquet.bouqet_sort(rose, tulips, tulips_1, choice='1')
print(bouquet.search_flower(rose, tulips, tulips_1))
bouquet_2 = Bouqet(rose, tulips, tulips_1)
print(rose in bouquet_2)
print(bouquet_2[0])
print(bouquet_2[1])
for flowers in bouquet_2:
    print(flowers)

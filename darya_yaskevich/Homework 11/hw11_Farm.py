from abc import ABC, abstractmethod


class NotExistException(Exception):
    def __init__(self, msg):
        super().__init__(msg)


class FarmCreatures(ABC):
    @abstractmethod
    def __init__(self, name, age):
        self.name = name
        self._age = age

    @abstractmethod
    def __str__(self):
        return f'{self.__class__.__name__} {self.name}'

    @abstractmethod
    def __getattr__(self, item):
        raise NotExistException(
            f'Attribute {item} does not exist for type {__class__.__name__}'
        )

    @property
    @abstractmethod
    def age(self):
        return self._age

    @abstractmethod
    def get_older(self):
        self._age += 1

    @abstractmethod
    def voice(self):
        print('I have a voice')


class Animal(FarmCreatures):
    @abstractmethod
    def walk(self):
        print('I can walk')


class Bird(FarmCreatures):
    @abstractmethod
    def fly(self):
        print('I can fly')


class Cow(Animal):
    def __init__(self, name, age):
        self.name = name
        self._age = age

    def __str__(self):
        return f'{self.__class__.__name__} {self.name}'

    def __getattr__(self, item):
        raise NotExistException(
            f'Type object "{__class__.__name__}"'
            f' has no attribute or method "{item}"'
        )

    @property
    def age(self):
        return self._age

    def get_older(self):
        self._age += 1

    def voice(self):
        print('му')

    def walk(self):
        print('I walk')


class Pig(Animal):
    def __init__(self, name, age):
        self.name = name
        self._age = age

    def __str__(self):
        return f'{self.__class__.__name__} {self.name}'

    def __getattr__(self, item):
        raise NotExistException(
            f'Type object "{__class__.__name__}"'
            f' has no attribute or method "{item}"'
        )

    @property
    def age(self):
        return self._age

    def get_older(self):
        self._age += 1

    def voice(self):
        print('хрю')

    def walk(self):
        print('I walk')


class Goose(Bird):
    def __init__(self, name, age):
        self.name = name
        self._age = age

    def __str__(self):
        return f'{self.__class__.__name__} {self.name}'

    def __getattr__(self, item):
        raise NotExistException(
            f'Type object "{__class__.__name__}"'
            f' has no attribute or method "{item}"'
        )

    @property
    def age(self):
        return self._age

    def get_older(self):
        self._age += 1

    def voice(self):
        print('гага')

    def fly(self):
        print('I fly')


class Chicken(Bird):
    def __init__(self, name, age):
        self.name = name
        self._age = age

    def __str__(self):
        return f'{self.__class__.__name__} {self.name}'

    def __getattr__(self, item):
        raise NotExistException(
            f'Type object "{__class__.__name__}"'
            f' has no attribute or method "{item}"'
        )

    @property
    def age(self):
        return self._age

    def get_older(self):
        self._age += 1

    def voice(self):
        print('кудах')

    def fly(self):
        print('I fly')


class Farm:
    def __init__(self, name, *animals):
        self.animals = animals
        self.name = name

    def __getitem__(self, index):
        return self.animals[index]

    def __iter__(self):
        return iter(self.animals)

    def increase_age(self):
        print('The age of all animals has been increased')
        for anim in self.animals:
            anim.get_older()


if __name__ == '__main__':
    cow = Cow('Mai', 3)
    pig = Pig('Peppa', 2)
    goose = Goose('Sally', 1)
    chicken = Chicken('Chick', 1)

    farm = Farm('Animal Farm', cow, pig, chicken)

    chicken.fly()
    cow.walk()
    pig.voice()
    cow.voice()
    goose.voice()
    chicken.voice()
    # увеличить возраст на 1
    print(goose.age)
    goose.get_older()
    print(goose.age)
    # обращение к несуществующему атрибуту (обработка ошибки)
    try:
        print(cow.farmer)
    except NotExistException:
        print('несуществующий атрибут или метод')
    # print(cow.farmer)

    # получить животное, живущее на ферме по индексу
    print(farm[2])
    # получить каждое животное на ферме через цикл for
    for animal in farm:
        print(animal)
    # увеличить возраст всех животных
    farm.increase_age()
    for animal in farm:
        print(animal.age)

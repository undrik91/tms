# 1. Напишите функцию-генератор, которая вычисляет числа фибоначчи.
def fib(n: int):
    f1, f2 = 0, 1
    for i in range(n):
        yield f1
        f1, f2 = f2, f1 + f2


# 2. Напишите генератор списка который принимает список
# numbers = [34.6, -203.4, 44.9, 68.3, -12.2, 44.6, 12.7]
# и возвращает новый список только с положительными числами.
def only_positive(lst: list):
    lst_positive = [i for i in lst if i > 0]
    yield lst_positive


# 3. Необходимо составить список чисел которые указывают на длину слов
# в строке: sentence = "the quick brown fox jumps over the lazy dog",
# но только если слово не "the".
def calculate_words(sentence: str):
    sent_lst = sentence.split()
    lst = [len(el) for el in sent_lst if el != 'the']
    yield lst


# 1. Программа получает на вход не возрастающую последовательность
# натуральных чисел, означающих рост каждого человека в строю.
# После этого вводится число X – рост Пети.
# Выведите номер, под которым Петя должен встать в строй.
# Если в строю есть люди с одинаковым ростом,таким же, как у Пети,
# то он должен встать после них.
# rank([165, 163, 162, 160, 157, 157, 155, 154], 162)  #=> 3


def rank(row: list, height: int):
    index_in_row = None
    for friends_height in row:
        if friends_height <= height:
            index_in_row = row.index(friends_height) + 1
            break
    return f'Номер в строю: {index_in_row + 1}'


print(rank([165, 163, 162, 160, 157, 157, 155, 154], 162))


# 2. Дан список чисел.
# Выведите все элементы списка, которые больше предыдущего элемента.
# [1, 5, 2, 4, 3]  #=> [5, 4]
# [1, 2, 3, 4, 5] #=> [2, 3, 4, 5]
numbers = [1, 2, 3, 4, 5]
result = [k for i, k in enumerate(numbers) if numbers[i] > numbers[i - 1]]
print(result)


# 3. Напишите программу, принимающую зубчатый массив любого типа
# и возвращающего его "плоскую" копию.
flat_lst = []


def flatten_list(lst: list):
    for el in lst:
        if isinstance(el, list):
            flatten_list(el)
        else:
            flat_lst.append(el)
    return flat_lst


my_list = [1, 2, [3, 4, [5, 6], 7], [8, [9, [10]]]]
print(flatten_list(my_list))


# 4. Напишите функцию, которая принимает на вход одномерный массив
# и два числа - размеры выходной матрицы. На выход программа должна подавать
# матрицу нужного размера, сконструированную из элементов массива.
def reshape(lst: list, rows: int, cols: int):
    lst_copy = lst[:]
    matrix = []
    for i in range(rows):
        matrix.append(lst_copy[:cols])
        del lst_copy[:cols]
    return matrix


print(reshape([1, 2, 3, 4, 5, 6, 7, 8], 4, 2))

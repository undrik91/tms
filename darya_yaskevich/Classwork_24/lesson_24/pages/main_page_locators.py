from selenium.webdriver.common.by import By


class MainPageLocators:
    LOGIN_OR_REGISTER_LINK = (By.CSS_SELECTOR, "#login_link")
    OFFERS_LINK = (By.XPATH, '//a[text()="Offers"]')
    BOOKS_LINK = (By.XPATH, '//a[text()="Books"]')

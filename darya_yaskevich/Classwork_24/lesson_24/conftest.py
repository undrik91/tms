import pytest
from selenium import webdriver


@pytest.fixture(scope="session")
def driver():
    driver = webdriver.Chrome('C:\\data\\chromedriver_win32\\chromedriver.exe')
    yield driver
    driver.quit()

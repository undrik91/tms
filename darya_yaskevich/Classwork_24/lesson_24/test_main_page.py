from pages.books_page import BooksPage
from pages.main_page import MainPage


def test_open_login_or_register_form(driver):
    page = MainPage(driver)
    page.open_page()
    login_page = page.open_login_page()
    login_page.login("Dumy", "password")

    assert login_page.should_be_login_page(), "Login page is not opened"


def test_open_offers_page(driver):
    page = MainPage(driver)
    page.open_page()
    page.open_offers()

    assert "/offers/" in driver.current_url, "Incorrect page has been opened"


def test_open_books_page(driver):
    page = BooksPage(driver)
    page.open_page()

    assert "Books" in page.get_page_title(), "Incorrect page has been opened"
